use std::time;

use crate::prisma::{self, blogs, PrismaClient};
use crate::types::Data;
use chrono::Utc;
use gql_client::Client;
use log::info;
use prisma_client_rust::NewClientError;
use tokio::time::sleep;

pub async fn spawn_scraper_thread() {
    let _worker_thread = tokio::spawn(async {
        let client: Result<PrismaClient, NewClientError> = prisma::new_client().await;
        loop {
            scrape_hashnode_api(client.as_ref().unwrap())
                .await
                .expect("Failed to scrape hashnode API");
            info!(
                "Updated Local Hashnode Blogs Database at {}.",
                Utc::now().timestamp()
            );
            sleep(time::Duration::from_secs(600)).await;
        }
    });
}

pub async fn scrape_hashnode_api(client: &PrismaClient) -> Result<(), Box<dyn std::error::Error>> {
    let query = r#"query {
      user(username: "rithulkamesh") {
         publication {
          posts(page: 0) {
            title
            brief
            slug
            coverImage
            popularity
          }
        }
      }
    }
    "#;

    let gql_client = Client::new("https://api.hashnode.com/");
    let response: Option<Data> = gql_client.query::<Data>(query).await.unwrap();
    for post in &response.unwrap().user.publication.posts {
        let _: blogs::Data = client
            .blogs()
            .upsert(
                blogs::slug::equals(post.slug.clone()),
                blogs::create(
                    post.slug.clone(),
                    post.title.clone(),
                    post.cover_image.clone(),
                    post.brief.clone(),
                    post.popularity,
                    vec![],
                ),
                vec![
                    blogs::title::set(post.title.clone()),
                    blogs::cover_image::set(post.cover_image.clone()),
                    blogs::brief::set(post.brief.clone()),
                    blogs::popularity::set(post.popularity),
                ],
            )
            .exec()
            .await
            .unwrap();
    }
    Ok(())
}
