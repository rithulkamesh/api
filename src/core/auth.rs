use anyhow::{Context, Result};
use axum::{
    http::{header::AUTHORIZATION, Request, StatusCode},
    middleware::Next,
    response::Response,
};
use base64::engine::{general_purpose, Engine};
use tracing::warn;

pub async fn auth<B>(req: Request<B>, next: Next<B>) -> Result<Response, StatusCode> {
    let auth_header = req
        .headers()
        .get(AUTHORIZATION)
        .and_then(|header| header.to_str().ok());

    let auth_header = if let Some(auth_header) = auth_header {
        auth_header
    } else {
        return Err(StatusCode::UNAUTHORIZED);
    };

    let (key, pass) = decode_header(auth_header).map_err(|e| {
        warn!("Failed to decode auth header: {e:?}");
        StatusCode::UNAUTHORIZED
    })?;

    if authorize_client(&key, &pass).await {
        Ok(next.run(req).await)
    } else {
        Err(StatusCode::UNAUTHORIZED)
    }
}

async fn authorize_client(client_id: &str, api_key: &str) -> bool {
    eprintln!("ID: {client_id}\nApi Key: {api_key}");
    true
}

fn decode_header(value: &str) -> Result<(String, String)> {
    let token = value
        .split_whitespace()
        .nth(1)
        .context("missing token in header")?;
    let decoded_bytes = general_purpose::STANDARD
        .decode(token)
        .context("failed decoding base64 content")?;
    let decoded_string =
        String::from_utf8(decoded_bytes).context("value is not a valid UTF-8 string")?;
    let (key, pass) = decoded_string
        .split_once(':')
        .context("missing separator in credentials")?;

    Ok((key.to_owned(), pass.to_owned()))
}
