use crate::{
    prisma::blogs,
    types::{AppJsonResult, AppState},
};
use axum::{
    extract::{Json, State},
    routing::get,
    Router,
};
use prisma_client_rust::Direction;

pub fn new() -> Router<AppState> {
    Router::new().route("/", get(get_pop_blogs))
}

async fn get_pop_blogs(State(state): State<AppState>) -> AppJsonResult<Vec<blogs::Data>> {
    let data = state
        .client
        .blogs()
        .find_many(vec![])
        .order_by(blogs::popularity::order(Direction::Desc))
        .take(5)
        .exec()
        .await
        .unwrap();

    Ok(Json::from(data))
}
