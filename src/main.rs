//! My Personal, Private api
//!
//! When I use a lot of services, Often their APIs have ratelimits which can be avoided by just
//! doing something once in a while and using my own API to serve it to my services without
//! ratelimits.
//!
mod core;
#[allow(warnings, unused)]
mod prisma;
mod routes;
mod types;

extern crate cronjob;

use std::net::Ipv4Addr;

use axum::{Router, middleware};
use prisma_client_rust::NewClientError;

use crate::core::hashnode::spawn_scraper_thread;
use crate::prisma::PrismaClient;
use crate::types::AppState;

static PORT: &str = "PORT";

#[tokio::main]
async fn main() {
    dotenvy::dotenv().expect("Failed to load env into memory");

    spawn_scraper_thread().await;
    let client: Result<PrismaClient, NewClientError> = prisma::new_client().await;

    let app_state = AppState::from(client);
    let app = Router::new()
        .nest("/api/v1/blogs", routes::blogs::new())
        .route_layer(middleware::from_fn(crate::core::auth::auth))
        .with_state(app_state);

    let port = std::env::var(PORT).unwrap_or_else(|_| String::from("3000")).parse::<u16>().expect("Failed to parse Port.");
    let addr = (Ipv4Addr::UNSPECIFIED, port).into();

    println!("App starting on http://0.0.0.0:{}", port);

    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await
        .expect("Failed to start server");
}
