use std::sync::Arc;

use crate::prisma::PrismaClient;
use axum::{
    http::StatusCode,
    response::{IntoResponse, Response},
    Json,
};
use prisma_client_rust::{
    prisma_errors::query_engine::{RecordNotFound, UniqueKeyViolation},
    NewClientError, QueryError,
};

#[derive(Clone)]
pub struct AppState {
    pub client: Arc<PrismaClient>,
}

impl From<Result<PrismaClient, NewClientError>> for AppState {
    fn from(client: Result<PrismaClient, NewClientError>) -> Self {
        AppState {
            client: Arc::new(client.unwrap()),
        }
    }
}

pub enum AppError {
    PrismaError(QueryError),
    NotFound,
}

impl From<QueryError> for AppError {
    fn from(error: QueryError) -> Self {
        match error {
            e if e.is_prisma_error::<RecordNotFound>() => AppError::NotFound,
            e => AppError::PrismaError(e),
        }
    }
}

pub type AppResult<T> = Result<T, AppError>;
pub type AppJsonResult<T> = AppResult<Json<T>>;

impl IntoResponse for AppError {
    fn into_response(self) -> Response {
        let status = match self {
            AppError::PrismaError(error) if error.is_prisma_error::<UniqueKeyViolation>() => {
                StatusCode::CONFLICT
            }
            AppError::PrismaError(_) => StatusCode::BAD_REQUEST,
            AppError::NotFound => StatusCode::NOT_FOUND,
        };

        status.into_response()
    }
}
