use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct BlogResponseItem {
    pub title: String,
    pub slug: String,
    pub cover_image: String,
    pub brief: String,
    pub popularity: f64,
}

#[derive(Deserialize, Debug)]
pub struct Pub {
    pub publication: Post,
}

#[derive(Deserialize, Debug)]
pub struct Post {
    pub posts: Vec<BlogResponseItem>,
}

#[derive(Deserialize, Debug)]
pub struct Data {
    pub user: Pub,
}

